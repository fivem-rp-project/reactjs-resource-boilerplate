import React, { useState, useEffect } from 'react';
import { emitClient } from './utils/fivem';

const Render = ({
  isOpen,
  text,
  onButtonClick,
}) => (
  <div>
    {isOpen && (
      <div>
        {text}
        <button onClick={onButtonClick}>click</button>
      </div>
    )}
  </div>
);

const App = () => {
  const [isOpen, setIsOpen] = useState(true);
  const [text, setText] = useState('Hello world');

  const callClient = () => {
    emitClient('resource_name', 'callBackFromNui', text);
  };
  const openUI = () => {
    setIsOpen(true);
  }
  const closeUI = () => {
    setIsOpen(false);
  }
  const listener = (event) => {
    const { action } = event.data;
    if (action === 'open') {
      // do something to open ui
      openUI();
    } else if (action === 'close') {
      closeUI();
    }
  }

  useEffect(() => {
    window.addEventListener('message', listener);
    return () => {
      window.removeEventListener('message', listener);
    };
  }, []);

  return (
    <Render
      isOpen={isOpen}
      text={text}
      onButtonClick={callClient}
    />
  );
};

export default App;
