(() => {

  const openNUI = () => {
    SetNuiFocus(true, true);
    SendNuiMessage(JSON.stringify({
      action: 'open',
    }));
  };

  const closeNUI = () => {
    SetNuiFocus(false, false);
    SendNuiMessage(JSON.stringify({
      action: 'close',
    }));
  }

  on('resource_name:event_name_open', () => {
    openNUI();
  });

  on('resource_name:event_name_close', () => {
    closeNUI();
  });

  RegisterNuiCallbackType('callbackFromNui')
  on('__cfx_nui:callbackFromNui', (data, cb) => {
    cb({ ok: true, data });
  });

})()