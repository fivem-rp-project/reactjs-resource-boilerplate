# reactjs-resource-boilerplate

> :warning: **cfx webpack module do not support right now!!**

This repository is used for template of FiveM resource that handle UI with `react.js`

## Event definition
```javascript
// listen resource event to send 'open' action to NUI
on('resource_name:event_name_open');

// listen resource event to send 'close' action to NUI
on('resource_name:event_name_close');
```

**NOTE** all script is written in `javascript` only.

